output "postgres-fqdn" {
  value = yandex_mdb_postgresql_cluster.postgres.host.0.fqdn
}

output "nginx-ip" {
  value = yandex_compute_instance.nginx.network_interface[0].nat_ip_address
}

output "instas-ip" {
  value = {
    for insta in yandex_compute_instance.instas:
    insta.hostname => insta.network_interface[0].ip_address
  }
}
