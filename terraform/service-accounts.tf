resource "yandex_iam_service_account" "image-puller-sa" {
  name      = "image-puller"
  folder_id = var.folder_id
}

resource "yandex_resourcemanager_folder_iam_binding" "image-puller-sa-b" {
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  members   = [
    "serviceAccount:${yandex_iam_service_account.image-puller-sa.id}",
  ]
}
