resource "yandex_mdb_postgresql_cluster" "postgres" {
  name        = "postgres"
  environment = "PRESTABLE"
  network_id  = yandex_vpc_network.tf-network.id

  config {
    version = 12

    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = 16
    }

    postgresql_config = {
      max_connections                   = 395
      enable_parallel_hash              = true
      vacuum_cleanup_index_scale_factor = 0.2
      autovacuum_vacuum_scale_factor    = 0.34
      default_transaction_isolation     = "TRANSACTION_ISOLATION_READ_COMMITTED"
      shared_preload_libraries          = "SHARED_PRELOAD_LIBRARIES_AUTO_EXPLAIN,SHARED_PRELOAD_LIBRARIES_PG_HINT_PLAN"
    }
  }

  database {
    name  = "insta_storage"
    owner = "insta_admin"
  }

  user {
    name       = "insta_admin"
    password   = "insta_password"
    conn_limit = 50

    permission {
      database_name = "insta_storage"
    }

    settings = {
      default_transaction_isolation = "read committed"
      log_min_duration_statement    = 5000
    }
  }

  host {
    zone      = var.zone
    subnet_id = yandex_vpc_subnet.tf-subnet.id
  }
}
