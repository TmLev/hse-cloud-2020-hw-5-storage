import os
import time

from pathlib import Path

import pytest
import requests
import boto3


THIS_MODULE_PATH = Path(__file__).resolve()
TESTS_DIR = THIS_MODULE_PATH.parent

SERVICE_URL = os.getenv('SERVICE_URL')

AWS_ENDPOINT = os.getenv('AWS_ENDPOINT')
AWS_BUCKET = os.getenv('AWS_BUCKET')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')


@pytest.fixture
def url():
    return "http://dummy-service:5000"


@pytest.fixture
def ensure_service_running(url):
    while True:
        try:
            response = requests.get(url)
            assert response.status_code == 200
            break
        except requests.exceptions.ConnectionError:
            print(f'Waiting for "{url}" to run...')
            time.sleep(1)


@pytest.fixture
def s3():
    session = boto3.Session(
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
    )
    return session.client(
        service_name="s3",
        endpoint_url=AWS_ENDPOINT,
    )


@pytest.fixture
def create_bucket(s3):
    try:
        s3.create_bucket(Bucket=AWS_BUCKET)
    except:
        pass


@pytest.fixture
def image():
    with open(TESTS_DIR / "image.png", "rb") as f:
        return f.read()


@pytest.mark.usefixtures("ensure_service_running", "create_bucket")
def test_service(url, s3, image):
    uploader_response = requests.post(
        url=f"{url}/api/uploader",
        files={"file": image},
    )
    assert uploader_response.status_code == 200

    image_url = uploader_response.json()["url"]
    response = requests.get(image_url)
    assert response.status_code == 200
    assert response.content == image

    objects = s3.list_objects(Bucket=AWS_BUCKET)
    s3_image = s3.get_object(
        Bucket=AWS_BUCKET,
        Key=objects["Contents"][0]["Key"],
    )["Body"].read()
    assert s3_image == image
