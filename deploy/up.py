#! /usr/bin/env python3

import json
import subprocess as sp
import logging as log

from pathlib import Path

from pssh.clients import ParallelSSHClient
from pssh.output import HostOutput


# ------------------------------------------------------------------------------

ROOT_DIR = Path(__file__).resolve().parent.parent

log.basicConfig(level="INFO")


# ------------------------------------------------------------------------------


def parse_terraform_output() -> dict[str, ...]:
    completed = sp.run(
        ["terraform", "output", "-json"],
        capture_output=True,
        cwd=ROOT_DIR / "terraform",
    )
    out = completed.stdout.decode()
    return json.loads(out)


# ------------------------------------------------------------------------------

def scp_files(client: ParallelSSHClient) -> list:
    files = [
        {
            "local_file": "project",
            "recurse":    True,
        },
        {
            "local_file":  ".env.remote",
            "remote_file": ".env",
        },
        {
            "local_file": "Dockerfile",
        },
        {
            "local_file": "requirements.txt",
        }
    ]

    handles = []

    for f in files:
        log.info(f"SCP file {f['local_file']}...")
        local = f["local_file"]
        hs: list = client.copy_file(
            local_file=local,
            remote_file=f.get("remote_file", local),
            recurse=f.get("recurse", False),
        )
        handles.extend(hs)

    return handles


# ------------------------------------------------------------------------------

def debug_ssh(hosts_outputs: list[HostOutput]) -> None:
    for host_output in hosts_outputs:
        print(f"Host {host_output.host} says:")
        for line in host_output.stdout:
            print(line)

        print(f"\nErrors:")
        for line in host_output.stderr:
            print(line)

        print()


# ------------------------------------------------------------------------------

def main() -> None:
    tf_output = parse_terraform_output()
    services = [
        tf_output["instas-ip"]["value"][f"insta-{i}"]
        for i in range(1, 3)
    ]

    client = ParallelSSHClient(
        hosts=services,
        proxy_host=tf_output["nginx-ip"]["value"],
        pkey="~/.ssh/cloud_rsa",
    )

    for handle in scp_files(client):
        handle.get()

    hosts_outputs: list[HostOutput] = client.run_command(
        command="docker build . -t service",
    )
    debug_ssh(hosts_outputs)

    hosts_outputs: list[HostOutput] = client.run_command(
        command="docker run --detach --env-file .env --network host service",
    )
    debug_ssh(hosts_outputs)


if __name__ == "__main__":
    main()
