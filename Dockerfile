# Base Python image
FROM python:3.9.1

# Install certificate for managed Postgres
RUN mkdir -p ~/.postgresql && \
    wget "https://storage.yandexcloud.net/cloud-certs/CA.pem" -O ~/.postgresql/root.crt && \
    chmod 0600 ~/.postgresql/root.crt

# Set working directory
WORKDIR /app

# Copy list of dependencies
COPY requirements.txt .

# Install dependencies
RUN pip install -r requirements.txt

# Copy everything else
COPY project project

# Set env variables
ENV FLASK_APP=project
ENV FLASK_DEBUG=True

# Run server
ENTRYPOINT flask run --host=0.0.0.0
