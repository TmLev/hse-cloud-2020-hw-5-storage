import logging as log
import uuid

from werkzeug.datastructures import FileStorage

from . import (
    s3,
    AWS_ENDPOINT,
    AWS_BUCKET,
)


log.basicConfig(level="INFO")


def push_to_storage(file: FileStorage) -> str:
    log.info(f"Pushing file {file.name} to storage")
    file_uuid = str(uuid.uuid4())
    s3.upload_fileobj(file.stream, AWS_BUCKET, file_uuid)
    url = f"{AWS_ENDPOINT}/{AWS_BUCKET}/{file_uuid}"
    return url
